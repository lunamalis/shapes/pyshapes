#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
"""Set up."""
from setuptools import setup


long_description: str = ''
with open('README.md') as f:
	long_description = f.read()

setup(
	name='lunamalis_pyshapes',
	version='0.0.1',
	description='Variety of shapes that are widely used in Lunamalis.',
	author='',
	long_description=long_description,
	long_description_content_type='text/markdown',
	url='https://gitlab.com/lunamalis/shapes/pyshapes'
)
