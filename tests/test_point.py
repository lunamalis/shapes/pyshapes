#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
"""Test points."""
import operator
from typing import Dict, Tuple, List, Callable

import lunamalis_pyshapes


def test_exists():
	assert hasattr(lunamalis_pyshapes, 'Point')
	assert isinstance(lunamalis_pyshapes.Point, object)


def test_create():
	test_point = lunamalis_pyshapes.Point(
		x=2,
		y=2
	)
	assert test_point
	assert test_point.x == 2
	assert test_point.y == 2
	test_point.set_to(0, 0)
	assert test_point
	assert test_point.x == 0
	assert test_point.y == 0
	assert test_point.pos == (0, 0)


def test_clone():
	test_point = lunamalis_pyshapes.Point(2, 5)
	assert test_point.clone() == test_point
	assert not test_point.clone() is test_point


def test_repr():
	for test in (
		(5, 5),
	):
		point = lunamalis_pyshapes.Point(
			x=test[0],
			y=test[1]
		)
		assert repr(point) == point.__repr__()
		assert point.__repr__() == 'Point(x={}, y={})'.format(test[0], test[1])
		assert eval(
			repr(point),
			None,
			{'Point': lunamalis_pyshapes.Point}
		) == point


def test_comparison():
	tests: Dict[str, Tuple[Tuple[int, int], Tuple[int, int], bool]] = {
		'lt': (
			((0, 0), (0, 0), False),
			((5, 5), (0, 0), False),
			((0, 0), (5, 5), True),
			((5, 0), (0, 0), False),
			((0, 5), (0, 0), False),
			((0, 0), (5, 0), False),
			((0, 0), (0, 5), False),
			((2, 2), (2, 3), False)
		),
		'le': (
			((0, 0), (0, 0), True),
		),
		'eq': (
			((0, 0), (0, 0), True),
		),
		'gt': (
			((0, 0), (0, 0), False),
		),
		'ge': (
			((0, 0), (0, 0), True),
		)
	}
	for test in tests['lt']:
		point_1 = lunamalis_pyshapes.Point(test[0][0], test[0][1])
		point_2 = lunamalis_pyshapes.Point(test[1][0], test[1][1])
		result = point_1 < point_2
		assert isinstance(result, bool)
		assert result is test[2]
	for test in tests['le']:
		point_1 = lunamalis_pyshapes.Point(test[0][0], test[0][1])
		point_2 = lunamalis_pyshapes.Point(test[1][0], test[1][1])
		result = point_1 <= point_2
		assert isinstance(result, bool)
		assert result is test[2]
	for test in tests['eq']:
		point_1 = lunamalis_pyshapes.Point(test[0][0], test[0][1])
		point_2 = lunamalis_pyshapes.Point(test[1][0], test[1][1])
		result = point_1 == point_2
		assert isinstance(result, bool)
		assert result is test[2]
	for test in tests['gt']:
		point_1 = lunamalis_pyshapes.Point(test[0][0], test[0][1])
		point_2 = lunamalis_pyshapes.Point(test[1][0], test[1][1])
		result = point_1 > point_2
		assert isinstance(result, bool)
		assert result is test[2]
	for test in tests['ge']:
		point_1 = lunamalis_pyshapes.Point(test[0][0], test[0][1])
		point_2 = lunamalis_pyshapes.Point(test[1][0], test[1][1])
		result = point_1 >= point_2
		assert isinstance(result, bool)
		assert result is test[2]


operator_tests: Dict[
	str,
	List[Tuple[Tuple[int, int]]]
] = {
	'add': [
		((3, 5), (5, 2)),
		((10, 10), (10, 10)),
		((2305902, 20349320), (-2349023, 9)),
		((1, 1), (1, 1)),
		((1, 1), (-1, 1)),
		((1, 1), (1, -1)),
		((1, 1), (-1, -1)),

		((-1, 1), (1, 1)),
		((-1, 1), (-1, 1)),
		((-1, 1), (1, -1)),
		((-1, 1), (-1, -1)),

		((1, -1), (1, 1)),
		((1, -1), (-1, 1)),
		((1, -1), (1, -1)),
		((1, -1), (-1, -1)),

		((-1, -1), (1, 1)),
		((-1, -1), (-1, 1)),
		((-1, -1), (1, -1)),
		((-1, -1), (-1, -1)),

		((2, 2), (2, 2)),
		((2, 2), (-2, 2)),
		((2, 2), (2, -2)),
		((2, 2), (-2, -2)),

		((-2, 2), (2, 2)),
		((-2, 2), (-2, 2)),
		((-2, 2), (2, -2)),
		((-2, 2), (-2, -2)),

		((2, -2), (2, 2)),
		((2, -2), (-2, 2)),
		((2, -2), (2, -2)),
		((2, -2), (-2, -2)),

		((-2, -2), (2, 2)),
		((-2, -2), (-2, 2)),
		((-2, -2), (2, -2)),
		((-2, -2), (-2, -2)),

		((0, 0), (0, 0))
	],
	# Subtraction, multiplication, exponentiation (power, a ** b),
	# bitwise and, bitwise xor, and bitwise or can all use the same numbers.
	# Therefore, after this dictionary, those items are added in.

	# True division, floor division, modulo, and divmod can not use the same
	# numbers as the items listed above, and so have their own set in the
	# case of division-by-zero errors.
	'truediv': [
		((3, 5), (2, 1)),

		((1, 1), (1, 1)),
		((1, 1), (-1, 1)),
		((1, 1), (1, -1)),
		((1, 1), (-1, -1)),

		((-1, 1), (1, 1)),
		((-1, 1), (-1, 1)),
		((-1, 1), (1, -1)),
		((-1, 1), (-1, -1)),

		((1, -1), (1, 1)),
		((1, -1), (-1, 1)),
		((1, -1), (1, -1)),
		((1, -1), (-1, -1)),

		((-1, -1), (1, 1)),
		((-1, -1), (-1, 1)),
		((-1, -1), (1, -1)),
		((-1, -1), (-1, -1)),

		((2, 2), (2, 2)),
		((2, 2), (-2, 2)),
		((2, 2), (2, -2)),
		((2, 2), (-2, -2)),

		((-2, 2), (2, 2)),
		((-2, 2), (-2, 2)),
		((-2, 2), (2, -2)),
		((-2, 2), (-2, -2)),

		((2, -2), (2, 2)),
		((2, -2), (-2, 2)),
		((2, -2), (2, -2)),
		((2, -2), (-2, -2)),

		((-2, -2), (2, 2)),
		((-2, -2), (-2, 2)),
		((-2, -2), (2, -2)),
		((-2, -2), (-2, -2)),
	],
	# For left shifts, the second point may not contain any negative
	# numbers. Therefore, they need their own set as well. Right shifts are
	# in the same case, and can use the same set of numbers.
	'lshift': [
		((3, 5), (1, 2)),
		((-3, 5), (1, 2)),

		((1, 1), (1, 1)),
		((-1, 1), (1, 1)),
		((1, -1), (1, 1)),
		((-1, -1), (1, 1)),

		((2, 2), (2, 2)),
		((-2, 2), (2, 2)),
		((2, -2), (2, 2)),
		((-2, -2), (2, 2)),
	]
}

operator_tests['sub'] = operator_tests['add']
operator_tests['mul'] = operator_tests['add']
operator_tests['pow'] = operator_tests['add']
operator_tests['and'] = operator_tests['add']
operator_tests['xor'] = operator_tests['add']
operator_tests['or'] = operator_tests['add']

operator_tests['floordiv'] = operator_tests['truediv']
operator_tests['mod'] = operator_tests['truediv']
operator_tests['divmod'] = operator_tests['truediv']

operator_tests['rshift'] = operator_tests['lshift']

operator_tests_operator: Dict[str, Callable] = {
	'add': operator.add,
	'sub': operator.sub,
	'mul': operator.mul,
	'truediv': operator.truediv,
	'floordiv': operator.floordiv,
	'mod': operator.mod,
	# Did you know that // and % are faster than divmod except when used against
	# really big numbers?
	'divmod': divmod,
	'pow': pow,
	'lshift': operator.lshift,
	'rshift': operator.rshift,
	'and': operator.and_,
	'xor': operator.xor,
	'or': operator.or_
}

operator_tests_operator_i: Dict[str, Callable] = {
	'add': operator.iadd,
	'sub': operator.isub,
	'mul': operator.imul,
	'truediv': operator.itruediv,
	'floordiv': operator.ifloordiv,
	'mod': operator.imod,
	'divmod': divmod,
	'pow': operator.ipow,
	'lshift': operator.ilshift,
	'rshift': operator.irshift,
	'and': operator.iand,
	'xor': operator.ixor,
	'or': operator.ior
}


def test_operators():
	for test_key, test_tests in operator_tests.items():
		for test in test_tests:
			op = operator_tests_operator[test_key]
			point_1 = lunamalis_pyshapes.Point(test[0][0], test[0][1])
			point_2 = lunamalis_pyshapes.Point(test[1][0], test[1][1])
			result = op(point_1, point_2)
			assert point_1.x == test[0][0]
			assert point_1.y == test[0][1]
			assert point_2.x == test[1][0]
			assert point_2.y == test[1][1]
			assert isinstance(result, lunamalis_pyshapes.Point)
			assert result.x == op(point_1.x, point_2.x)
			assert result.y == op(point_1.y, point_2.y)


def test_operator_self():
	"""TODO: """
	return
	for test_key, test_tests in operator_tests.items():
		for test in test_tests:
			iop = operator_tests_operator_i[test_key]
			point_1 = lunamalis_pyshapes.Point(test[0][0], test[0][1])
			point_2 = lunamalis_pyshapes.Point(test[1][0], test[1][1])
			# This "*should*" be equivalent to
			# `point_1 <op>= point_2`
			point_1 = iop(point_1, point_2)
			print(
				iop,
				test[0][0],
				test[0][1],
				'/',
				point_2.x,
				point_2.y,
				'->',
				point_1.x,
				point_1.y
			)
			if test[0][0] != 0:
				assert point_1.x != test[0][1]

			if test[0][1] != 0:
				assert point_1.y != test[0][1]


def test_negative():
	for test in (
		(0, 3),
	):
		point = lunamalis_pyshapes.Point(test[0], test[1])
		negative_point = -point
		assert -negative_point == point


def test_positive():
	for test in (
		(5, 2),
		(-2, -5),
		(-3, 2),
		(9, -2)
	):
		point = lunamalis_pyshapes.Point(test[0], test[1])
		positive_point = +point
		assert positive_point.x == +point.x
		assert positive_point.y == +point.y
		# Assert that the original point hasn't changed.
		assert point.x == test[0]
		assert point.y == test[1]


def test_abs():
	for test in (
		(-2, 1),
	):
		point = lunamalis_pyshapes.Point(test[0], test[1])
		point_abs = abs(point)
		assert point_abs is not point
		assert point_abs == abs(point)
		assert point_abs.x == abs(point.x)
		assert point_abs.y == abs(point.y)
		# Assert that the original point hasn't changed.
		assert point.x == test[0]
		assert point.y == test[1]


def test_invert():
	for test in (
		(5, 3),
	):
		point = lunamalis_pyshapes.Point(test[0], test[1])
		point_inverted = ~point
		assert point_inverted is not point
		assert point_inverted == ~point
		# In case the invertion worked on ourself, reinvert.
		~point
		assert point_inverted.x == ~point.x
		assert point_inverted.y == ~point.y
		# Assert that the original hasn't changed.
		assert point.x == test[0]
		assert point.y == test[1]


def test_round():
	for test in (
		(3.2, 5.5, None),
	):
		point = lunamalis_pyshapes.Point(test[0], test[1])
		point_rounded = round(point, test[2])
		print(point, '->', point_rounded)
		assert point_rounded is not point
		assert point_rounded.x == round(point.x, test[2])
		assert point_rounded.y == round(point.y, test[2])
		# Assert that the original hasn't changed.
		assert point.x == test[0]
		assert point.y == test[1]


def test_trunc():
	import math
	for test in (
		(9.3, 52.1),
	):
		point = lunamalis_pyshapes.Point(test[0], test[1])
		point_trunc = math.trunc(point)
		assert point_trunc.x == math.trunc(point.x)
		assert point_trunc.y == math.trunc(point.y)
		# Assert that original point hasn't changed.
		assert point.x == test[0]
		assert point.y == test[1]


def test_floor():
	import math
	for test in (
		(9.2, 53.1),
	):
		point = lunamalis_pyshapes.Point(test[0], test[1])
		point_floor = math.floor(point)
		assert point_floor.x == math.floor(point.x)
		assert point_floor.y == math.floor(point.y)
		# Assert that the original point hasn't changed
		assert point.x == test[0]
		assert point.y == test[1]


def test_ceil():
	import math
	for test in (
		(9.9, 1.35),
	):
		point = lunamalis_pyshapes.Point(test[0], test[1])
		point_ceil = math.ceil(point)
		assert point_ceil.x == math.ceil(point.x)
		assert point_ceil.y == math.ceil(point.y)
		# Assert that the original hasn't changed.
		assert point.x == test[0]
		assert point.y == test[1]


def test_set_to():
	for test in (
		(23, 10, 3, 5),
	):
		point = lunamalis_pyshapes.Point(test[0], test[1])
		point.set_to(test[2], test[3])
		assert point.x == test[2]
		assert point.y == test[3]


def test_clone():
	for test in (
		(2, 3),
	):
		point = lunamalis_pyshapes.Point(test[0], test[1])
		point_clone = point.clone()
		assert point_clone == point
		assert point_clone is not point


def test_invert_points():
	for test in (
		(5, 3),
	):
		point = lunamalis_pyshapes.Point(test[0], test[1])
		result = point.invert_points()
		assert point.x == test[1]
		assert point.y == test[0]
		assert result == point
		assert result is point


def test_slope():
	for test in (
		(3, 5, 7, 9),
	):
		point_1 = lunamalis_pyshapes.Point(test[0], test[1])
		point_2 = lunamalis_pyshapes.Point(test[2], test[3])
		slope = point_1.slope(point_2)
		# Slope formula is (y_2 - y_1) / (x_2 - x_1)
		assert slope == ((point_2.y - point_1.y) / (point_2.x - point_1.x))


def test_clamp():
	for test in (
		(3, 5, -2, 5),
		(0, 0, 0, 0),
		(0, 0, -5, -5),
		(5, 2, -1, 3)
	):
		point = lunamalis_pyshapes.Point(test[0], test[1])
		result = point.clamp(test[2], test[3])
		assert result == point
		assert result is point
		assert point.x >= test[2]
		assert point.x <= test[3]
		assert point.y >= test[2]
		assert point.y <= test[3]


def test_magnitude():
	import math
	for test in (
		(3, 5, 9),
	):
		point = lunamalis_pyshapes.Point(test[0], test[1])
		mag = point.magnitude
		assert mag == math.sqrt((point.x * point.x) + (point.y * point.y))
		# Now squared
		assert mag == math.sqrt(point.magnitude_squared)
		# Now setter
		point.magnitude = test[2]
		assert point.x == (test[0] * test[2])
		assert point.y == (test[1] * test[2])


def test_normalize():
	for test in (
		(3, 5),
	):
		point = lunamalis_pyshapes.Point(test[0], test[1])
		original_magnitude = point.magnitude
		result = point.normalize()
		assert result is point
		if test[0] == 0 and test[1] == 0:
			assert result.x == (test[0] / original_magnitude)
			assert result.y == (test[1] / original_magnitude)


def test_limit():
	for test in (
		(5, 2, 1),
	):
		point = lunamalis_pyshapes.Point(test[0], test[1])
		result = point.limit(test[2])
		assert result is point


def test_expand():
	pass


def test_dot():
	pass


def test_cross():
	pass


def test_perpendicular_point():
	for test in (
		(2, 3),
	):
		point = lunamalis_pyshapes.Point(test[0], test[1])
		perp = point.perpendicular_point
		assert perp is not point
		assert perp.perpendicular_point_reversed == point
		assert perp.x == -point.y
		assert perp.y == point.x

		reversed_perp = point.perpendicular_point_reversed
		assert reversed_perp is not point
		assert reversed_perp.perpendicular_point == point
		assert reversed_perp.x == point.y
		assert reversed_perp.y == -point.x


def test_pos():
	for test in (
		(3, 5),
	):
		point = lunamalis_pyshapes.Point(test[0], test[1])
		assert hasattr(point, 'pos')
		pos = point.pos
		assert isinstance(pos, tuple)
		assert len(pos) == 2
		assert pos[0] == test[0]
		assert pos[1] == test[1]
		assert pos == test
