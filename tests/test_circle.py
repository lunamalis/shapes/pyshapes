#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
"""Test the circle shape.

For testing, make a link to your virtual environment import path.

You can do this by, for example, running:

```
ln -s $PWD/lunamalis_pyshapes \
$PWD/.venv/lib/python3.7/site-packages/lunamalis_pyshapes
```
"""
import pytest


def test_import():
	import lunamalis_pyshapes
