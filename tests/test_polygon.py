#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
"""Polygons."""
import lunamalis_pyshapes


def test_exists():
	assert hasattr(lunamalis_pyshapes, 'Polygon')
	assert isinstance(lunamalis_pyshapes.Polygon, object)


def test_create():
	test_poly = lunamalis_pyshapes.Polygon(
		lunamalis_pyshapes.Point(3, 5),
		lunamalis_pyshapes.Point(2, 9)
	)
	assert test_poly


def test_area():
	# We'll create a simple rectangle.
	test_rect = lunamalis_pyshapes.Polygon(
		lunamalis_pyshapes.Point(5, 5),
		lunamalis_pyshapes.Point(10, 5),
		lunamalis_pyshapes.Point(10, 10),
		lunamalis_pyshapes.Point(5, 10)
	)
	# The area should be this.
	area = 25
	assert test_rect.area == area


def test_extremes():
	test_poly = lunamalis_pyshapes.Polygon(
		lunamalis_pyshapes.Point(2, 5),
		lunamalis_pyshapes.Point(10, 5),
		lunamalis_pyshapes.Point(10, 12),
		lunamalis_pyshapes.Point(5, 12)
	)
	assert test_poly.top == 12
	assert test_poly.bottom == 5
	assert test_poly.left == 2
	assert test_poly.right == 10


def test_from_list():
	test_1 = lunamalis_pyshapes.Polygon(
		lunamalis_pyshapes.Point(5, 5),
		lunamalis_pyshapes.Point(10, 5),
		lunamalis_pyshapes.Point(10, 10),
		lunamalis_pyshapes.Point(5, 10)
	)
	test_2 = lunamalis_pyshapes.Polygon.from_list([5, 5, 10, 5, 10, 10, 5, 10])
	assert test_1 is not test_2
	assert test_1.points == test_2.points


def test_from_args():
	test_1 = lunamalis_pyshapes.Polygon(
		lunamalis_pyshapes.Point(5, 5),
		lunamalis_pyshapes.Point(10, 5),
		lunamalis_pyshapes.Point(10, 10),
		lunamalis_pyshapes.Point(5, 10)
	)
	test_2 = lunamalis_pyshapes.Polygon.from_args(5, 5, 10, 5, 10, 10, 5, 10)
	assert test_1 is not test_2
	assert test_1.points == test_2.points


def test_eq():
	pass


# def test_convex():
# 	pass


def test_points_are_clockwise():
	points = (
		lunamalis_pyshapes.Point(5, 5),
		lunamalis_pyshapes.Point(10, 5),
		lunamalis_pyshapes.Point(10, 10),
		lunamalis_pyshapes.Point(5, 10)
	)
	assert lunamalis_pyshapes.Polygon.points_are_clockwise(points)


def test_angles():
	test_poly = lunamalis_pyshapes.Polygon(
		lunamalis_pyshapes.Point(5, 5),
		lunamalis_pyshapes.Point(10, 5),
		lunamalis_pyshapes.Point(10, 10),
		lunamalis_pyshapes.Point(5, 10)
	)
	angles = test_poly.interior_angles_sum
	assert angles == 360
