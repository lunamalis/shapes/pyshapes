#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
"""Line."""
from typing import Union
import math

import random
import lunamalis_pyshapes


class Line:
	def __init__(
		self,
		start: lunamalis_pyshapes.Point,
		end: lunamalis_pyshapes.Point
	):
		"""Initialize.

		:param start:
		:param end:
		"""
		self.start: lunamalis_pyshapes.Point = start
		self.end: lunamalis_pyshapes.Point = end

	@staticmethod
	def from_xy(x1, y1, x2, y2) -> 'Line':
		"""Create a Line using easy points."""
		return Line(
			lunamalis_pyshapes.Point(x1, y1),
			lunamalis_pyshapes.Point(x2, y2)
		)

	def __bool__(self):
		"""Determine if the line has a length."""
		# A line without a length is considered to be false.
		# If both points are the same, then there is no distance between them.
		return self.start != self.end

	def __len__(self):
		"""Return the distance between the start and end points."""
		return self.distance(self.start, self.end)

	@property
	def x(self) -> Union[int, float]:
		"""Return and set the central x position."""
		return (self.start.x + self.end.x) / 2

	@x.setter
	def x(self, value: Union[int, float]):
		self.start.x += value
		self.end.x += value

	@property
	def y(self) -> Union[int, float]:
		"""Return and set the central y position."""
		return (self.start.y + self.end.y) / 2

	@y.setter
	def y(self, value: Union[int, float]):
		self.start.y += value
		self.end.y += value

	@property
	def position(self) -> lunamalis_pyshapes.Point:
		"""Return the central point."""
		return lunamalis_pyshapes.Point(self.x, self.y)

	@property
	def angle(self) -> Union[int, float]:
		"""Return the angle of the line."""
		return math.atan2(self.end.y - self.start.y, self.end.x - self.start.x)

	@property
	def slope(self) -> Union[int, float]:
		"""Return the slope of the line."""
		return (self.end.y - self.start.y) / (self.end.x - self.start.y)

	@property
	def perpendicular_slope(self) -> Union[int, float]:
		"""Return the slope that is perpendicular to this line's slope."""
		return -(
			(self.end.x - self.start.x) / (self.end.y - self.start.y)
		)

	@property
	def left(self) -> Union[int, float]:
		"""Return the leftmost, smallest x value of this line."""
		return min(self.start.x, self.end.x)

	@property
	def right(self) -> Union[int, float]:
		"""Return the rightmost, highest x value of this line."""
		return max(self.start.x, self.end.x)

	@property
	def top(self) -> Union[int, float]:
		"""Return the topmost, highest y value of this line."""
		return min(self.start.y, self.end.y)

	@property
	def bottom(self) -> Union[int, float]:
		"""Return the bottommost, lowest y value of this line."""
		return max(self.start.y, self.end.y)

	@property
	def width(self) -> Union[int, float]:
		"""Return the width of this line."""
		return abs(self.start.x - self.end.x)

	@property
	def height(self) -> Union[int, float]:
		"""Return the height of this line."""
		return abs(self.start.y - self.end.y)

	@property
	def normal_x(self) -> Union[int, float]:
		"""Return the normal x of this line."""
		return math.cos(self.angle - math.pi / 2)

	@property
	def normal_y(self) -> Union[int, float]:
		"""Return the normal y of this line."""
		return math.sin(self.angle - math.pi / 2)

	@property
	def normal_angle(self) -> Union[int, float]:
		"""Return the normal angle of this line."""
		# TODO: ...
		range_of = -2 * math.pi
		result = (self.angle + math.pi / 2) % range_of
		if result < 0:
			result += range_of
		return result - math.pi

	def set_to(
		self,
		x1: Union[int, float],
		y1: Union[int, float],
		x2: Union[int, float],
		y2: Union[int, float]
	) -> 'Line':
		"""Set the values of this line."""
		self.start.set_to(x1, y1)
		self.end.set_to(x2, y2)
		return self

	def clone(self):
		"""Return a new clone of this line.

		The start and end points will be cloned.
		"""
		return Line(self.start.clone(), self.end.clone())

	def rotate(self, angle: Union[int, float], degrees: bool = False) -> 'Line':
		"""Rotate this line."""
		center_x = self.x
		center_y = self.y
		self.rotate_around(center_x, center_y, angle, degrees)
		return self

	def rotate_around(
		self,
		x: Union[int, float],
		y: Union[int, float],
		angle: Union[int, float],
		degrees: bool = False
	):
		"""Rotate this line around a specific point."""
		self.start.rotate(x, y, angle, degrees)
		self.end.rotate(x, y, angle, degrees)
		return self

	def reflect(self, other: 'Line') -> Union[int, float]:
		"""Reflect this line."""
		# TODO: since pi is an irrational number (though it can't be
		# represented that way), doesn't that mean that this can only ever
		# return floats?
		return 2 * other.normal_angle - math.pi - self.angle

	def intersects(self, other: 'Line') -> bool:
		"""Determine if this line intersects another."""
		return self._intersects_do_intersect(
			self.start, self.end,
			other.start, other.end
		)

	def _intersects_on_segment(
		self,
		first: lunamalis_pyshapes.Point,
		between: lunamalis_pyshapes.Point,
		last: lunamalis_pyshapes.Point
	) -> bool:
		"""Determine if all three points are on a line.

		Determine if the `between` point is directly between the `first` and
		`last` points.
		"""
		if (
			between.x <= max(first.x, last.x)
			and between.x >= min(first.x, last.x)
			and between.y <= max(first.y, last.y)
			and between.y >= min(first.y, last.y)
		):
			return True
		return False

	def _intersects_orientation(
		self,
		first: lunamalis_pyshapes.Point,
		between: lunamalis_pyshapes.Point,
		last: lunamalis_pyshapes.Point
	):
		value: Union[int, float] = (
			(between.y - first.y) * (last.x - between.x)
			- (between.x - first.x) * (last.y - between.y)
		)

		# TODO:
		# assert value >= 0
		if value == 0:
			return 0
		elif value > 0:
			return 1
		else:
			return 2

	def _intersects_do_intersect(
		self,
		p1: lunamalis_pyshapes.Point,
		q1: lunamalis_pyshapes.Point,
		p2: lunamalis_pyshapes.Point,
		q2: lunamalis_pyshapes.Point
	) -> bool:
		"""With great help from Fabian Ying.

		See here:
		https://stackoverflow.com/a/47080549
		http://web.archive.org/web/20190513174309/https://stackoverflow.com/questions/3838329/how-can-i-check-if-two-segments-intersect
		"""
		o1 = self._intersects_orientation(p1, q1, p2)
		o2 = self._intersects_orientation(p1, q1, q2)
		o3 = self._intersects_orientation(p2, q2, p1)
		o4 = self._intersects_orientation(p2, q2, q1)

		if o1 != o2 and o3 != o4:
			return True

		# Special cases
		# p1, q1, and p2 are colinear. p2 lies on segment p1<->q1
		if o1 == 0 and self._intersects_on_segment(p1, p2, q1):
			return True

		# p1, q1, and p2 are colinear. q2 lies on segment p1<->q1
		if o2 == 0 and self._intersects_on_segment(p1, q2, q1):
			return True

		# p2, q2, and p1 are colinear. p1 lies on segment p2q2.
		if o3 == 0 and self._intersects_on_segment(p2, p1, q2):
			return True

		# p2, q2, and q1 are colinear. q1 lies on segment p2<->q2
		if o4 == 0 and self._intersects_on_segment(p2, q1, q2):
			return True

		return False

	def center_on(self, point: lunamalis_pyshapes.Point) -> 'Line':
		"""Center the line on a point."""
		diff_x = point.x - self.x
		diff_y = point.y - self.y
		self.start.x += diff_x
		self.start.y += diff_y
		self.end.x += diff_x
		self.end.y += diff_y

		return self

	def point_on_line(self, x, y, epsilon=0) -> bool:
		"""Determine if a given point is on this line."""
		return (
			abs(
				(x - self.start.x) * (self.end.y - self.start.y)
				- (self.end.x - self.start.x) * (y - self.start.y)
			) < epsilon
		)

	def random(self) -> lunamalis_pyshapes.Point:
		"""Return a random point on this line."""
		rnd: float = random.random()
		return lunamalis_pyshapes.Point(
			x=self.start.x + (self.end.x - self.start.x) * rnd,
			y=self.start.y + (self.end.y - self.start.y) * rnd
		)

	def distance(self, p1, p2) -> Union[int, float]:
		"""Return the distance between two points."""
		x_mult = (p2.x - p1.x)
		y_mult = (p2.y - p1.y)
		return math.sqrt(
			x_mult * x_mult + y_mult * y_mult
		)

	def angle_between(self, other: 'Line') -> Union[int, float]:
		"""Return the angle between two lines."""
		slope_1 = self.slope
		slope_2 = other.slope
		return (slope_2 - slope_1) / (1 + slope_2 * slope_1)
