#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
"""Rectangles are two dimensional planes or something."""
from typing import Union, Tuple
import random

import lunamalis_pyshapes


class Rectangle:
	"""Rectangles are two dimensional planes."""

	def __init__(
		self,
		x: Union[int, float],
		y: Union[int, float],
		width: Union[int, float] = 0,
		height: Union[int, float] = 0
	):
		"""Initialize.

		:param x:
		:param y:
		:param width:
		:param height:
		"""
		self.x: Union[int, float] = x
		self.y: Union[int, float] = y
		self.width: Union[int, float] = width
		self.height: Union[int, float] = height

	@property
	def pos(self) -> Tuple[Union[int, float], Union[int, float]]:
		"""Return a tuple containing the `x` and `y` positions."""
		return (self.x, self.y)

	@property
	def point(self) -> lunamalis_pyshapes.Point:
		"""Return a Point containing the `x` and `y` positions."""
		return lunamalis_pyshapes.Point(self.x, self.y)

	@property
	def top(self) -> Union[int, float]:
		"""Return the topmost y-point of the rectangle."""
		return self.y

	@property
	def bottom(self) -> Union[int, float]:
		"""Return the bottommost y-point of the rectangle."""
		return self.y + self.height

	@property
	def left(self) -> Union[int, float]:
		"""Return the leftmost x-point of the rectangle."""
		return self.x

	@property
	def right(self) -> Union[int, float]:
		"""Return the rightmost x-point of the rectangle."""
		return self.x + self.width

	@property
	def top_left(self) -> lunamalis_pyshapes.Point:
		"""Return the topleft point of the rectangle."""
		return self.point

	@property
	def top_right(self) -> lunamalis_pyshapes.Point:
		"""Return the topright point of the rectangle."""
		return lunamalis_pyshapes.Point(self.x + self.width, self.y)

	@property
	def bottom_left(self) -> lunamalis_pyshapes.Point:
		"""Return the bottomleft point of the rectangle."""
		return lunamalis_pyshapes.Point(self.x, self.y + self.height)

	@property
	def bottom_right(self) -> lunamalis_pyshapes.Point:
		"""Return the bottomright point of the rectangle."""
		return lunamalis_pyshapes.Point(
			self.x + self.width,
			self.y + self.height
		)

	@property
	def center(self) -> lunamalis_pyshapes.Point:
		"""Return a Point at the center of the rectangle."""
		return lunamalis_pyshapes.Point(
			self.center_x,
			self.center_y
		)

	@property
	def center_x(self):
		"""Return the `x` value at the center of the rectangle."""
		return self.x + self.width / 2

	@property
	def center_y(self):
		"""Return the `y` value at the center of the rectangle."""
		return self.y + self.height / 2

	@property
	def empty(self) -> bool:
		"""Determine if the rectangle is empty or not."""
		if self.width or self.height:
			return True
		return False

	@empty.setter
	def empty(self, value: bool) -> None:
		"""Empty the rectangle, setting its width and height to zero."""
		if value:
			self.width = 0
			self.height = 0

	@property
	def size(self) -> lunamalis_pyshapes.Point:
		"""Return a Point containing the width and height of the rectangle."""
		return lunamalis_pyshapes.Point(self.width, self.height)

	@property
	def volume(self) -> Union[int, float]:
		"""Return the volume of the rectangle."""
		return self.width * self.height

	@property
	def perimeter(self) -> Union[int, float]:
		"""Return the perimeter of the rectangle."""
		return self.width * 2 + self.height * 2

	@property
	def random(self) -> lunamalis_pyshapes.Point:
		"""Return a random point within the rectangle."""
		return lunamalis_pyshapes.Point(self.random_x, self.random_y)

	@property
	def random_x(self):
		"""Return a random `x` value within the rectangle."""
		return random.random() * self.width + self.x

	@property
	def random_y(self):
		"""Return a random `y` value within the rectangle."""
		return random.random() * self.height + self.y

	def set_to(
		self,
		x: Union[int, float],
		y: Union[int, float],
		width: Union[int, float],
		height: Union[int, float]
	) -> 'Rectangle':
		"""Set the rectangle's primary properties."""
		self.x = x
		self.y = y
		self.width = width
		self.height = height
		return self

	def clone(self) -> 'Rectangle':
		"""Return an exact clone of this rectangle."""
		return Rectangle(self.x, self.y, self.width, self.height)

	def contains(
		self,
		x: Union[int, float],
		y: Union[int, float]
	) -> bool:
		"""Determine if a given point is within the rectangle.

		:param x:
		:param y:
		"""
		return (
			x >= self.x and x < self.right
			and y >= self.y and y < self.bottom
		)

	def contains_rectangle(self, rect: 'Rectangle') -> bool:
		"""Determine if a given rectangle is within this rectangle.

		:param rect:
		"""
		# If our volume is lesser, than we can not possibly contain the other.
		if self.volume < rect.volume:
			return False

		return (
			self.x >= rect.x and self.y >= rect.y
			and self.right < rect.right and self.bottom < rect.bottom
		)
