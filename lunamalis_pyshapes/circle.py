#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
"""Circles are circles."""
from typing import Union
import random

import math
from . import point


class Circle:
	"""Circles are, well, circles."""

	def __init__(
		self,
		x: Union[int, float] = 0,
		y: Union[int, float] = 0,
		diameter: Union[int, float] = 0
	):
		"""Initialize.

		:param x:
		:param y:
		:param diameter:
		"""
		self.x: Union[int, float] = x
		self.y: Union[int, float] = y
		self._diameter: Union[int, float] = diameter
		self._radius: Union[int, float] = diameter / 2

	@property
	def diameter(self) -> Union[int, float]:
		"""Get the diameter of the circle."""
		return self._diameter

	@diameter.setter
	def diameter(self, value: Union[int, float]) -> None:
		"""Set the diameter of the circle.

		This also sets the radius to half of the given value.
		"""
		self._diameter = value
		self._radius = value / 2

	@property
	def radius(self) -> Union[int, float]:
		"""Get the radius of the circle."""
		return self._radius

	@radius.setter
	def radius(self, value: Union[int, float]) -> None:
		"""Set the radius of the circle.

		This will also set the diameter to be twice of the given value.
		"""
		self._radius = value
		self._diameter = value * 2

	@property
	def circumference(self) -> Union[int, float]:
		"""Get the distance around the circle."""
		return math.pi * self._radius * 2

	@property
	def area(self) -> Union[int, float]:
		"""Get the total surface area of the circle."""
		return math.pi * self._radius * self._radius

	@property
	def empty(self) -> bool:
		"""Determine if the diameter of the circle is zero or not."""
		return self._diameter == 0

	@property
	def left(self) -> Union[int, float]:
		"""Get the least `x` value of the circle."""
		return self.x - self._radius

	@property
	def right(self) -> Union[int, float]:
		"""Get the greatest `x` value of the circle."""
		return self.x + self._radius

	@property
	def top(self) -> Union[int, float]:
		"""Get the lowest `y` value of the circle."""
		return self.y - self._radius

	@property
	def bottom(self) -> Union[int, float]:
		"""Get the greatest `y` value of the circle."""
		return self.y + self._radius

	def __lt__(self, other: 'Circle') -> bool:
		"""Determine if another circle is less than this circle."""
		return (
			self.x < other.x
			and self.y < other.y
			and self.diameter < other.diameter
		)

	def __le__(self, other: 'Circle') -> bool:
		"""Determine if another circle is less than or equal to this circle."""
		return (
			self.x <= other.x
			and self.y <= other.y
			and self.diameter <= other.diameter
		)

	def __eq__(self, other: 'Circle') -> bool:
		"""Determine if another circle is equivalent to this circle."""
		# TODO: Debate on whether positions of objects have anything to do with
		# descriptions of objects, and then debate on whether the `==` operator
		# cares about only the position of objects, descriptions of objects, or
		# both. (80 characters 3 times in a row!)
		return (
			self.x == other.x
			and self.y == other.y
			and self.diameter == other.diameter
		)

	def __gt__(self, other: 'Circle') -> bool:
		"""Determine if another circle is greater than this circle."""
		return not self.__lt__(other=other)

	def __ge__(self, other: 'Circle') -> bool:
		"""Determine if another circle is greater than or equal to this one."""
		return not self.__le__(other=other)

	@classmethod
	def has_similar_attrs(cls, other: object) -> bool:
		"""Determine if another object is similar to a circle.

		This is mainly used in the equivalency checkers.
		"""
		if isinstance(other, cls):
			return True
		if (
			hasattr(other, 'x') and hasattr(other, 'y')
			and hasattr(other, 'diameter')
		):
			return True
		return False

	def set_to(
		self,
		x: Union[int, float],
		y: Union[int, float],
		diameter: Union[int, float]
	) -> 'Circle':
		"""Set the values of the circle.

		:param x:
		:param y:
		:param diameter:
		"""
		self.x = x
		self.y = y
		self.diameter = diameter
		self.radius = diameter / 2
		return self

	def clone(self) -> 'Circle':
		"""Return an exact clone of this circle.

		If this circle's `x`, `y`, or `diameter` values are mutable objects of
		some kind, then both the clone and the original may be affected if the
		values are changed on either of them.
		"""
		return Circle(self.x, self.y, self.diameter)

	def random(self) -> point.Point:
		"""Return a random point within the circle's surface."""
		t = 2 * math.pi * random.random()
		u = random.random() + random.random()
		r = u
		if u > 1:
			r = 2 - u
		x = r * math.cos(t)
		y = r * math.sin(t)
		return point.Point(
			x=self.x + (x * self.radius),
			y=self.y + (y * self.radius)
		)

	def contains(self, x: Union[int, float], y: Union[int, float]) -> bool:
		"""Determine if a given point is within this circle."""
		# Something without a volume can not contain anything.
		if self.radius <= 0:
			return False

		if self.left <= x and self.right >= x and self.top <= y and self.bottom >= y:
			diff_x: Union[int, float] = (self.x - x) ** 2
			diff_y: Union[int, float] = (self.y - y) ** 2
			return (diff_x + diff_y) <= (self.radius ** 2)
		return False

	def circumference_point(
		self, angle: Union[int, float], degrees: bool = False
	) -> point.Point:
		"""Return a point on the circle's circumference given an angle.

		:param angle:
		:param degrees: Whether to use degrees or, by default, radians.
		"""
		if degrees:
			angle = angle * 180 / math.pi
		return point.Point(
			x=self.x + self.radius * math.cos(angle),
			y=self.y + self.radius * math.sin(angle)
		)
