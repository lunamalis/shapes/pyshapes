#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

from typing import Union
import random

from . import point
from . import line


class Triangle:

	def __init__(
		self,
		p1: point.Point,
		p2: point.Point,
		p3: point.Point
	):
		self.p1 = p1
		self.p2 = p2
		self.p3 = p3

	def __lt__(self, other: 'Triangle') -> bool:
		return (
			self.p1 < other.p1
			and self.p2 < other.p2
			and self.p3 < other.p3
		)

	def __le__(self, other: 'Triangle') -> bool:
		return (
			self.p1 <= other.p1
			and self.p2 <= other.p2
			and self.p3 <= other.p3
		)

	def __eq__(self, other: 'Triangle') -> bool:
		return (
			self.p1 == other.p1
			and self.p2 == other.p2
			and self.p3 == other.p3
		)

	def __gt__(self, other: 'Triangle') -> bool:
		return not self.__lt__(other=other)

	def __ge__(self, other: 'Triangle') -> bool:
		return not self.__le__(other=other)

	@property
	def left(self):
		return min(self.p1.x, self.p2.x, self.p3.x)

	@property
	def right(self):
		return max(self.p1.x, self.p2.x, self.p3.x)

	@property
	def top(self):
		return min(self.p1.y, self.p2.y, self.p3.y)

	@property
	def bottom(self):
		return max(self.p1.y, self.p2.y, self.p3.y)

	@property
	def area(self):
		return abs(
			(
				(self.p3.x - self.p1.x) * (self.p2.y - self.p1.y)
				- (self.p2.x - self.p1.x) * (self.p3.y - self.p1.y)
			)
			/ 2
		)

	@property
	def centroid(self):
		"""The center of mass for the triangle."""
		return point.Point(
			x=(self.p1.x + self.p2.x + self.p3.x) / 3,
			y=(self.p1.y + self.p2.y + self.p3.y) / 3
		)

	@property
	def line_1(self):
		return line.Line(self.p1.x, self.p1.y, self.p2.x, self.p2.y)

	@property
	def line_2(self):
		return line.Line(self.p2.x, self.p2.y, self.p3.x, self.p3.y)

	@property
	def line_3(self):
		return line.Line(self.p3.x, self.p3.y, self.p1.x, self.p1.y)

	@property
	def perimeter(self):
		return (
			len(self.line_1)
			+ len(self.line_2)
			+ len(self.line_3)
		)

	@property
	def points(self):
		return (self.p1, self.p2, self.p3)

	@property
	def lines(self):
		return (self.line_1, self.line_2, self.line_3)

	def set_to(
		self,
		x1: Union[int, float],
		y1: Union[int, float],
		x2: Union[int, float],
		y2: Union[int, float],
		x3: Union[int, float],
		y3: Union[int, float]
	) -> 'Triangle':
		self.p1.set_to(x1, y1)
		self.p2.set_to(x2, y2)
		self.p3.set_to(x3, y3)
		return self

	def clone(self):
		return Triangle(
			self.p1.x, self.p1.y,
			self.p2.x, self.p2.y,
			self.p3.x, self.p3.y
		)

	def random(self):
		random_1: float = random.random()
		random_2: float = random.random()

		if random_1 + random_2 >= 1:
			random_1 = 1 - random_1
			random_2 = 1 - random_2

		return point.Point(
			x=(
				self.p1.x
				+ (
					(self.p2.x - self.p1.x) * random_1
					+ (self.p3.x - self.p1.x) * random_2
				)
			),
			y=(
				self.p1.x
				+ (
					(self.p2.y - self.p1.y) * random_1
					+ (self.p3.y - self.p1.y) * random_2
				)
			)
		)
