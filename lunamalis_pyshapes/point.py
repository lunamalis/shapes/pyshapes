#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
"""Points are 0-dimensional locations in a 2D plane."""
from typing import Union, Tuple
import math


class Point:
	"""Points are 0-dimensional locations in a 2D plane."""

	def __init__(self, x: Union[int, float], y: Union[int, float]):
		"""Initialize.

		:param x:
		:param y:
		"""
		self.x: Union[int, float] = x
		self.y: Union[int, float] = y

	def __repr__(self) -> str:
		"""Return the internal representation of this point."""
		return 'Point(x={}, y={})'.format(self.x, self.y)

	def __str__(self) -> str:
		"""Return a user-friendly representation of this point."""
		return 'Point at {}, {}'.format(self.x, self.y)

	def __lt__(self, other: 'Point') -> bool:
		"""Determine if `other` point is south-west of this point.

		This will determine if the other point's position is south-west of this
		point.
		"""
		return self.x < other.x and self.y < other.y

	def __le__(self, other: 'Point') -> bool:
		"""Determine if `other` point is less than or equal to this point.

		This will determine if the other point's position is south-west of this
		point, or aligned on the `x` and/or `y` properties.
		"""
		return self.x <= other.x and self.y <= other.y

	def __eq__(self, other: 'Point') -> bool:
		"""Compare if `other` point is in the same position as this point."""
		return self.x == other.x and self.y == other.y

	def __gt__(self, other: 'Point') -> bool:
		"""Determine if `other` point is greater than this point.

		This will determine if the other point's position is north-east of this
		point.
		"""
		return self.x > other.x and self.y > other.y

	def __ge__(self, other: 'Point') -> bool:
		"""Determine if `other` point is greater than or equal to this point.

		This will determine if the other point's position is north-east of this
		point, or aligned on the `x` and/or `y` properties.
		"""
		return self.x >= other.x and self.y >= other.y

	def __add__(self, other: 'Point') -> 'Point':
		return Point(self.x + other.x, self.y + other.y)

	def __sub__(self, other: 'Point') -> 'Point':
		return Point(self.x - other.x, self.y - other.y)

	def __mul__(self, other: 'Point') -> 'Point':
		return Point(self.x * other.x, self.y * other.y)

	def __truediv__(self, other: 'Point') -> 'Point':
		return Point(self.x / other.x, self.y / other.y)

	def __floordiv__(self, other: 'Point') -> 'Point':
		return Point(self.x // other.x, self.y // other.y)

	def __mod__(self, other: 'Point') -> 'Point':
		return Point(self.x % other.x, self.y % other.y)

	def __divmod__(self, other: 'Point') -> 'Point':
		return Point(divmod(self.x, other.x), divmod(self.y, other.y))

	def __pow__(self, other: 'Point', modulo: int = 0) -> 'Point':
		if modulo:
			return Point(
				(self.x ** other.x) % modulo, (self.y ** other.y) % modulo
			)
		return Point(self.x ** other.x, self.y ** other.y)

	def __lshift__(self, other: 'Point') -> 'Point':
		return Point(self.x << other.x, self.y << other.y)

	def __rshift__(self, other: 'Point') -> 'Point':
		return Point(self.x >> other.x, self.y >> other.y)

	def __and__(self, other: 'Point') -> 'Point':
		return Point(self.x & other.x, self.y & other.y)

	def __xor__(self, other: 'Point') -> 'Point':
		return Point(self.x ^ other.x, self.y ^ other.y)

	def __or__(self, other: 'Point') -> 'Point':
		return Point(self.x | other.x, self.y | other.y)

	def __iadd__(self, other: 'Point') -> 'Point':
		self.x += other.x
		self.y += other.y
		return self

	def __isub__(self, other: 'Point') -> 'Point':
		self.x -= other.x
		self.y -= other.y
		return self

	def __imul__(self, other: 'Point') -> 'Point':
		self.x *= other.x
		self.y *= other.y
		return self

	def __itruediv__(self, other: 'Point') -> 'Point':
		self.x /= other.x
		self.y /= other.y
		return self

	def __ifloordiv__(self, other: 'Point') -> 'Point':
		self.x //= other.x
		self.y //= other.y
		return self

	def __imod__(self, other: 'Point') -> 'Point':
		self.x %= other.x
		self.y %= other.y
		return self

	def __ipow__(self, other: 'Point', modulo: int = 0) -> 'Point':
		if modulo:
			raise NotImplementedError
			self.x = (self.x ** other.x) % modulo
			self.y = (self.y ** other.y) % modulo
		self.x **= other.x
		self.y **= other.y
		return self

	def __ilshift__(self, other: 'Point') -> 'Point':
		self.x <<= other.x
		self.y <<= other.y
		return self

	def __irshift__(self, other: 'Point') -> 'Point':
		self.x >>= other.x
		self.y >>= other.y
		return self

	def __iand__(self, other: 'Point') -> 'Point':
		self.x &= other.x
		self.y &= other.y
		return self

	def __ixor__(self, other: 'Point') -> 'Point':
		self.x ^= other.x
		self.y ^= other.y
		return self

	def __ior__(self, other: 'Point') -> 'Point':
		self.x |= other.x
		self.y |= other.y
		return self

	def __neg__(self) -> 'Point':
		return Point(-self.x, -self.y)

	def __pos__(self) -> 'Point':
		return Point(+self.x, +self.y)

	def __abs__(self) -> 'Point':
		return Point(abs(self.x), abs(self.y))

	def __invert__(self) -> 'Point':
		return Point(~self.x, ~self.y)

	def __round__(self, ndigits: int = None) -> 'Point':
		return Point(
			x=round(self.x, ndigits=ndigits),
			y=round(self.y, ndigits=ndigits)
		)

	def __trunc__(self) -> 'Point':
		return Point(
			x=math.trunc(self.x),
			y=math.trunc(self.y)
		)

	def __floor__(self) -> 'Point':
		return Point(
			x=math.floor(self.x),
			y=math.floor(self.y)
		)

	def __ceil__(self) -> 'Point':
		return Point(
			x=math.ceil(self.x),
			y=math.ceil(self.y)
		)

	def set_to(self, x: Union[int, float], y: Union[int, float]) -> 'Point':
		self.x = x
		self.y = y
		return self

	def clone(self) -> 'Point':
		return Point(self.x, self.y)

	def invert_points(self) -> 'Point':
		x = self.x
		y = self.y
		self.x = y
		self.y = x
		return self

	def slope(self, other: 'Point') -> Union[int, float]:
		return (other.y - self.y) / (other.x - self.x)

	def clamp(
		self,
		mini: Union[int, float],
		maxi: Union[int, float]
	) -> 'Point':
		if self.x > maxi:
			self.x = maxi
		elif self.x < mini:
			self.x = mini

		if self.y > maxi:
			self.y = maxi
		elif self.y < mini:
			self.y = mini

		return self

	@property
	def magnitude(self) -> Union[int, float]:
		return math.sqrt((self.x * self.x) + (self.y * self.y))

	@magnitude.setter
	def magnitude(self, value):
		self.normalize()
		self.x *= value
		self.y *= value
		return self

	@property
	def magnitude_squared(self) -> Union[int, float]:
		return (self.x * self.x) + (self.y * self.y)

	def normalize(self) -> 'Point':
		if self.x == 0 and self.y == 0:
			m = self.magnitude
			self.x /= m
			self.y /= m
		return self

	def limit(self, maxi: Union[int, float]) -> 'Point':
		if self.magnitude_squared > (maxi * maxi):
			self.magnitude = maxi
		return self

	def expand(self, mini: Union[int, float]) -> 'Point':
		if self.magnitude_squared < (mini * mini):
			self.magnitude = mini
		return self

	def dot(self, other: 'Point') -> Union[int, float]:
		return self.x * other.x + self.y * other.y

	def cross(self, other: 'Point') -> Union[int, float]:
		return self.x * other.x - self.y * other.y

	@property
	def perpendicular_point(self) -> 'Point':
		return Point(-self.y, self.x)

	@property
	def perpendicular_point_reversed(self) -> 'Point':
		return Point(self.y, -self.x)

	@property
	def pos(self) -> Tuple[Union[int, float], Union[int, float]]:
		return (self.x, self.y)


class PointUnlimited:
	"""Not fully implemented."""

	def __init__(self, **positions: dict):
		"""

		`positions` looks something like:

		```python3
		point_unlimited = PointUnlimited(
			x=1,
			y=2,
			z=3,
			w=4
		)
		```

		and etc.
		"""
		self.keys = positions.keys()
		for key, value in positions.items():
			setattr(self, key, value)

	def __repr__(self):
		returner = []
		for key in self.keys:
			returner.append('{}={}'.format(key, getattr(self, key)))
		return (
			'PointUnlimited('
			+ ', '.join(returner)
			+ ')'
		)
