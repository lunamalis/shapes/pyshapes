#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
"""Polygon."""
from typing import Union, List
import lunamalis_pyshapes


class Polygon:
	"""Polygons are collections of points, connected by lines."""

	def __init__(self, *points):
		"""Initialize.

		:param points: Integers or floats. Input them in clockwise order. (And
			maybe counter-clockwise is okay, too.)
		"""
		self._dirty: bool = True
		self.area: Union[int, float] = 0
		self.points: List[lunamalis_pyshapes.Point] = points
		self.clean()

	@property
	def top(self) -> Union[int, float]:
		"""Return the highest, largest y-value in the polygon."""
		if self._dirty:
			highest: lunamalis_pyshapes.Point = lunamalis_pyshapes.Point(
				0, float('-inf')
			)
			for point in self.points:
				if highest.y < point.y:
					highest = point
			self._top = highest.y
		return self._top

	@property
	def bottom(self) -> Union[int, float]:
		"""Return the lowest, smallest y-value in the polygon."""
		if self._dirty:
			self.clean()
		return self._bottom

	@property
	def left(self) -> Union[int, float]:
		"""Return the leftmost, smallest x-value in the polygon."""
		if self._dirty:
			self.clean()
		return self._left

	@property
	def right(self) -> Union[int, float]:
		"""Return the rightmost, largest x-value in the polygon."""
		if self._dirty:
			self.clean()
		return self._right

	@property
	def lines(self) -> List[lunamalis_pyshapes.Line]:
		"""Return a list of Lines connecting each point in a direction.

		Each point should have two lines each.

		This value is recalculated each time you call it.
		"""
		lines: List[lunamalis_pyshapes.Line] = []
		for index, point in enumerate(self.points):
			next_point = self.points[(index + 1) % len(self.points)]
			lines.append(lunamalis_pyshapes.Line(point, next_point))
		return lines

	@property
	def interior_angles(self) -> List[Union[int, float]]:
		"""Return a list of interior angles."""
		lines = self.lines
		angles: List[Union[int, float]] = []
		for index, line in enumerate(lines):
			next_line = lines[(index + 1) % len(lines)]
			angles.append(line.angle_between(next_line))
		return angles

	@property
	def interior_angles_sum(self) -> Union[int, float]:
		"""Return the sum of the interior angles."""
		return sum(self.interior_angles)

	def set_to(self, *points):
		"""Set the points of this polygon."""
		self.area = 0
		self.points = points

	def clone(self):
		"""Return an exact clone of this polygon.

		The clone should be an exact copy, and it copies the points as well.
		"""
		return Polygon(*self.points)

	def _clean_area_mult(self, point, next_point) -> Union[int, float]:
		"""Recalculate the area."""
		return ((point.x * next_point.y) - (point.y * next_point.x))

	def _clean_extremes(self, top, bottom, left, right, point):
		# print(
		# 	'x({left}, {right}), y({top}, {bottom}): p({point})'.format(
		# 		top=top, bottom=bottom, left=left, right=right, point=point
		# 	)
		# )
		if point.y > top:
			top = point.y
		if point.y < bottom:
			bottom = point.y
		if point.x > right:
			right = point.x
		if point.x < left:
			left = point.x
		return (top, bottom, left, right)

	def clean(self):
		"""Clean up the polygon.

		This undirties the polygon.
		"""
		self._dirty = True

		area: Union[int, float] = 0
		top: Union[int, float] = float('-inf')
		bottom: Union[int, float] = float('inf')
		right: Union[int, float] = float('-inf')
		left: Union[int, float] = float('inf')

		for index, point in enumerate(self.points):
			# Get the area first.
			next_point = self.points[(index + 1) % len(self.points)]
			area += self._clean_area_mult(point, next_point)

			# Now find extremes.
			top, bottom, left, right = self._clean_extremes(
				top, bottom, left, right, point
			)

		self.area = area / 2

		# Extremes.
		self._top = top
		self._bottom = bottom
		self._left = left
		self._right = right

		self._dirty = False

	def clean_area(self) -> Union[int, float]:
		"""Clean up just the area.

		Note that this does not undirty the polygon.
		"""
		area: Union[int, float] = 0

		for index, point in enumerate(self.points):
			next_point = self.points[(index + 1) % len(self.points)]
			area += self._clean_area_mult(point, next_point)
		self.area = area / 2
		return self.area

	def clean_extremes(self):
		"""Clean up just the extremes.

		Note that this does not undirty the polygon.
		"""
		top: Union[int, float] = float('-inf')
		bottom: Union[int, float] = float('inf')
		left: Union[int, float] = float('-inf')
		right: Union[int, float] = float('inf')

		for point in self.points:
			top, bottom, left, right = self._clean_extremes(
				top, bottom, left, right, point
			)
		self._top = top
		self._bottom = bottom
		self._left = left
		self._right = right

	def force_undirty(self):
		"""Force `self._dirty` to be `False`.

		Only use this if you know what you're doing.
		"""
		self.dirty = False

	@staticmethod
	def from_list(lst: List[Union[int, float]]):
		"""Return a polygon created from a list of generic integers/floats."""
		point_list: List[lunamalis_pyshapes.Point] = []
		# Loop over every two elements in the list.
		for x, y in zip(lst[0::2], lst[1::2]):
			point_list.append(lunamalis_pyshapes.Point(x, y))
		return Polygon(*point_list)

	@staticmethod
	def from_args(*lst: Union[int, float]):
		"""Return a polygon created from a list of generic integers/floats.

		Example:
		```
		Polygon.from_args(3, 5, 8, 9)
		```
		"""
		return Polygon.from_list(list(lst))

	def calculate_area(self):
		"""Calculate the area of the polygon.

		The polygon's area is equal to:
		```
		area = (
			(x_1 * y_2 - y_1 * x_2)
			+ (x_2 * y_3 - y_2 * x_3)
			+ (x_3 * y_4 - y_3 * x_4) ...
			+ (x_n * y_1 + y_n * x_1)
		) / 2
		```
		"""
		area = 0
		for index, point in enumerate(self.points):
			next_point = self.points[(index + 1) % len(self.points)]
			result = ((point.x * next_point.y) - (point.y * next_point.x))
			area += result
		area = area / 2
		return area

	def is_convex(self) -> bool:
		"""Determine if this polygon is convex or not.

		Wikipedia determines a convex polygon as when "any line drawn through
		the polygon (and not tangent to an edge or corner) meets its boundary
		exactly twice."
		(https://en.wikipedia.org/wiki/Polygon)
		(https://web.archive.org/web/20190611195239/https://en.wikipedia.org/wiki/Polygon)

		All the interior angles must be less than 180 degrees. Additionally,
		according to Wikipedia, "... any line segment with endpoints on the
		boundary passes through only interior points between its endpoints."
		"""
		raise NotImplementedError

		# Quick check; if the polygon has 2 points it definitely is not convex.
		# Or a polygon?
		if len(self.points) < 3:
			return False

	@staticmethod
	def points_are_clockwise(points) -> bool:
		"""Determine if the given points are given clockwise or not.

		With help from:
		https://stackoverflow.com/questions/1165647/how-to-determine-if-a-list-of-polygon-points-are-in-clockwise-order
		https://web.archive.org/web/20190611203336/https://stackoverflow.com/questions/1165647/how-to-determine-if-a-list-of-polygon-points-are-in-clockwise-order
		"""
		result: Union[int, float] = 0
		for index, point in enumerate(points):
			next_point = points[(index + 1) % len(points)]
			result += (next_point.x - point.x) * (next_point.y + point.y)
		if result < 0:
			# If negative, is counter-clockwise.
			# So return False.
			# TODO:
			return True
		return False

	@property
	def equiangular(self) -> bool:
		"""Determine if this polygon is equiangular.

		When a polygon is equiangular, all corner angles are equal.
		"""
